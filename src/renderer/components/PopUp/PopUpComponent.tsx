import React, {useEffect} from "react";
import {Typography} from "@material-ui/core";
import {useParams} from 'react-router-dom';
import PopupDialogueComponent from "../PopupDialogue/PopupDialogueComponent";


const PopUpComponent: React.FC = () => {
    const {imageName} = useParams();
    const popUpString = `File Loaded`;
    useEffect(() => {
        document.title = popUpString;
    }, []);
    return (
        <PopupDialogueComponent filePath={decodeURIComponent(imageName)}
                                open={true}
                                fullScreen={true}
                                handleClose={() => window.close()}
        />
    )
}

export default PopUpComponent;