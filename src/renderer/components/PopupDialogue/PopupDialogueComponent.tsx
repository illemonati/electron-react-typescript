import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";

interface PopupDialogueComponentProps {
    filePath: string,
    open: boolean,
    handleClose: () => void,
    fullScreen?: boolean,
}

const PopupDialogueComponent: React.FC<PopupDialogueComponentProps> = (props: PopupDialogueComponentProps) => {
    const open = props.open;
    const handleClose = props.handleClose;
    const filePath = props.filePath;
    const fullScreen = props.fullScreen || false;
    return (
        <Dialog open={open} onClose={handleClose} fullScreen={fullScreen}>
            <DialogTitle>
                File Loaded
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    The file at "{filePath}" has been loaded.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary" variant="contained">
                    Ok
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default PopupDialogueComponent;



