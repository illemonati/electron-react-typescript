import React, {useEffect, useState} from 'react';
import {remote} from 'electron';

import {Typography, Container, Button, Paper, Grid, Switch, FormControlLabel} from '@material-ui/core';
import './styles.css';
import fs from 'fs';
import mime from 'mime-types';
import PopupDialogueComponent from "../PopupDialogue/PopupDialogueComponent";

const HomePageComponent: React.FC = () => {
    const [imageSrc, setImageSrc] = useState<null | string>(null);
    const [openPopUpInExternalWindow, setOpenPopUpInExternalWindow] = useState(false);
    const [popUpDialogState, setPopUpDialogState] = useState({
        open: false,
        filePath: '',
    });

    useEffect(() => {
        document.title = "Main Page";
        //@ts-ignore
    }, []);

    const handleImageChange = async (imagePath: string) => {
        const mimeType = mime.lookup(imagePath);
        if (mimeType && mimeType.includes('image')) {
            openPopUp(imagePath).then();
            const img = fs.readFileSync(imagePath).toString('base64');
            const imgSrcString = `data:${mimeType};base64,${img}`;
            setImageSrc(imgSrcString);
        }
    };

    const openPopUp = async (imagePath: string) => {
        if (openPopUpInExternalWindow) {
            const win = new remote.BrowserWindow({
                height: 300,
                show: false,
            });
            win.loadURL(`${window.location.origin}/${remote.getGlobal('baseURL')}#popup/${encodeURIComponent(imagePath)}`);
            win.once('ready-to-show', () => win.show());
        } else {
            setPopUpDialogState(state => {
                state.open = true;
                state.filePath = imagePath;
                return {...state};
            })
        }
    }

    const handleImageChoose = () => {
        const dialogConfig = {
            properties: ['openFile']
        } as Electron.OpenDialogOptions;
        remote.dialog.showOpenDialog(dialogConfig, fileNames => {
            handleImageChange(fileNames[0]).then();
        });
    };
    return (
        <div className="HomePageComponent">
            <Container>
                <Typography variant="h2">Display Image</Typography>
                <br/>
                <br/>
                <Paper variant="outlined">
                    <br/>
                    <Grid container justify="center" alignItems="center" spacing={5}>
                        <Grid item xs={12}>
                            <Button onClick={handleImageChoose} variant="contained" color="primary">
                                Choose Image
                            </Button>
                        </Grid>
                        <Grid item>
                            <FormControlLabel
                                control={
                                    <Switch checked={openPopUpInExternalWindow}
                                            onChange={(e) => setOpenPopUpInExternalWindow(e.target.checked)}
                                            color="secondary"
                                    />
                                }
                                labelPlacement="start"
                                label="Open Popup in External Window"
                            />
                        </Grid>
                    </Grid>
                    <br/>
                </Paper>
                <br/>
                <br/>
                {imageSrc && (
                    <Paper variant="outlined">
                        <br/>
                        <Container>
                            <img className="mainImage" src={imageSrc} alt="chosen"/>
                        </Container>
                        <br/>
                        <br/>
                    </Paper>
                )}
            </Container>
            <PopupDialogueComponent filePath={popUpDialogState.filePath}
                                    open={popUpDialogState.open}
                                    handleClose={() => setPopUpDialogState(state => {
                                        state.open = false;
                                        return {...state};
                                    })}
            />
        </div>
    );
};

export default HomePageComponent;
