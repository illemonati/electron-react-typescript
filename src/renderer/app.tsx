import React from 'react';
import ReactDOM from 'react-dom';
// import { AppContainer } from 'react-hot-loader';
import CssBaseline from '@material-ui/core/CssBaseline';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import {remote} from 'electron';
import './styles.css';
import {
    HashRouter as Router,
    Switch,
    Route,
    // Link
} from "react-router-dom";
import PopUpComponent from "./components/PopUp/PopUpComponent";
import HomePageComponent from './components/HomePage/HomePageComponent';


// Create main element
const mainElement = document.createElement('div');
document.body.appendChild(mainElement);

const theme = createMuiTheme({
    palette: {
        type: 'dark',
    }
});

const IndexComponent: React.FC = () => {
    return (
        <MuiThemeProvider theme={theme}>
            <CssBaseline>
                <Router basename={remote.getGlobal('baseURL')}>
                    <Switch>
                        <Route path="/popup/:imageName">
                            <PopUpComponent />
                        </Route>
                        <Route path="/">
                            <HomePageComponent />
                        </Route>
                    </Switch>
                </Router>
            </CssBaseline>
        </MuiThemeProvider>
    );
};

ReactDOM.render(<IndexComponent />, mainElement);
